# Changelog

## 4.0.0 - 2023-06-04

### Changed

- Compatible SPIP 4.0->4.2
