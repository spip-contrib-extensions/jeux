<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
#---------------------------------------------------#
#  Plugin  : Jeux                                   #
#  Auteur  : Patrice Vanneufville, 2006             #
#  Gestion des scores : Maieul Rouquette, 2007      #
#  Contact : patrice¡.!vanneufville¡@!laposte¡.!net #
#  Licence : GPL                                    #
#--------------------------------------------------------------------------#
#  Documentation : https://contrib.spip.net/Des-jeux-dans-vos-articles  #
#--------------------------------------------------------------------------#

# exemples de jeux avec les mots :
# http://www.cc-concarneaucornouaille.fr/net_bibli/portail/jouer%20avec%20les%20mots.htm
# http://d.ch.free.fr/index.html
# jeux musicaux :
# http://www.metronimo.com/fr/jeux/

// balises du plugin a inserer dans les articles
define('_JEUX_DEBUT', '<jeux>');
define('_JEUX_FIN', '</jeux>');
define('_JEUX_POST', '@@JEUX_POST@@');
define('_JEUX_HEAD1', '<!-- CSS/JS JEUX -->');
define('_JEUX_HEAD2', '<!-- CSS/JS JEUX (AUTO) -->');


// separateurs utilisables a l'interieur des balises ci-dessus
// format a utiliser dans la redaction : [separateur]
define('_JEUX_TITRE', 'titre');			// separateur indiquant le titre du jeu
define('_JEUX_TEXTE', 'texte');			// separateur indiquant un contenu a garder telquel
define('_JEUX_COPYRIGHT', 'copyright');	// separateur indiquant un contenu a encapsuler dans un <div class="jeux_copyright"/>
define('_JEUX_CONFIG', 'config');		// separateur permettant de passer des parametres au jeu
#define('_JEUX_CONFIG', 'input');		// separateur permettant d'inserer un <input/> non evalue
define('_JEUX_REPONSE', 'reponse');
define('_JEUX_SOLUTION', 'solution');
define('_JEUX_SCORE', 'score');
define('_JEUX_HORIZONTAL', 'horizontal');
define('_JEUX_VERTICAL', 'vertical');
define('_JEUX_SUDOKU', 'sudoku');
define('_JEUX_KAKURO', 'kakuro');
define('_JEUX_QCM', 'qcm');
define('_JEUX_QRM', 'qrm');
define('_JEUX_QUIZ', 'quiz');
define('_JEUX_RELIER', 'relier');
define('_JEUX_GAUCHE', 'gauche');
define('_JEUX_DROITE', 'droite');
define('_JEUX_CHARADE', 'charade');
define('_JEUX_DEVINETTE', 'devinette');
define('_JEUX_TROU', 'trou');
define('_JEUX_POESIE', 'poesie');
define('_JEUX_CITATION', 'citation');
define('_JEUX_BLAGUE', 'blague');
define('_JEUX_AUTEUR', 'auteur');
define('_JEUX_RECUEIL', 'recueil');
define('_JEUX_PENDU', 'pendu');
define('_JEUX_DIAG_ECHECS', 'diag_echecs');
define('_JEUX_COLORATION', 'coloration');
define('_JEUX_CHESSSTUFF', 'chesstuff');
define('_JEUX_SAISIE', 'saisie');
define('_JEUX_LABEL', 'label');
define('_JEUX_MULTI_JEUX', 'jeu');

// globale stockant les carateristiques d'un jeu :
//   - les separateurs autorises
//   - les signatures permettant de reconnaitre un jeu
//   - le nom du jeu
// multi_jeux doit toujours etre en premier...
global $jeux_caracteristiques;
$jeux_caracteristiques = [
	// liste des separateurs autorises dans les jeux.
	// tous les jeux doivent etre listes ci-apres.
	// monjeu est le jeu traite dans le fichier jeux/monjeu.php
	'SEPARATEURS' => [
		'multi_jeux' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_MULTI_JEUX, _JEUX_CONFIG, _JEUX_SCORE],
		'sudoku' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_SUDOKU, _JEUX_SOLUTION, _JEUX_CONFIG],
		//'kakuro' => array(_JEUX_TITRE, _JEUX_TEXTE, _JEUX_KAKURO, _JEUX_SOLUTION, _JEUX_CONFIG),
		'mots_croises' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_HORIZONTAL, _JEUX_VERTICAL, _JEUX_SOLUTION, _JEUX_CONFIG],
		'qcm' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_QCM, _JEUX_QRM, _JEUX_QUIZ, _JEUX_CONFIG, _JEUX_SCORE],
		'textes' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_POESIE, _JEUX_CITATION, _JEUX_BLAGUE, _JEUX_AUTEUR, _JEUX_RECUEIL, _JEUX_CONFIG],
		'devinettes' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_DEVINETTE, _JEUX_CHARADE, _JEUX_REPONSE, _JEUX_CONFIG],
		'trous' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_TROU, _JEUX_CONFIG, _JEUX_SCORE],
		'pendu' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_PENDU, _JEUX_CONFIG],
		'diag_echecs' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_DIAG_ECHECS, _JEUX_COLORATION, _JEUX_CONFIG],
		'chesstuff' => [_JEUX_CHESSSTUFF, _JEUX_CONFIG],
		'saisies' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_LABEL, _JEUX_SAISIE, _JEUX_CONFIG],
		'relier' => [_JEUX_TITRE, _JEUX_TEXTE, _JEUX_COPYRIGHT, _JEUX_GAUCHE, _JEUX_DROITE, _JEUX_SOLUTION, _JEUX_CONFIG],
	],

	// liste des signatures caracteristiques d'un jeu.
	// tous les jeux doivent etre listes ci-apres.
	// monjeu est le jeu traite dans le fichier jeux/monjeu.php
	// exemple :
	// array(_JEUX_SEPAR_3, _JEUX_SEPAR_4) doit s'interpreter :
	// " le jeu est charge si on trouve _JEUX_SEPAR_3 ou _JEUX_SEPAR_4
	//   a l'interieur de <jeux> et </jeux> "
	'SIGNATURES' => [
		'multi_jeux' => [_JEUX_MULTI_JEUX],
		'sudoku' => [_JEUX_SUDOKU],
		//'kakuro' => array(_JEUX_KAKURO),
		'mots_croises' => [_JEUX_HORIZONTAL, _JEUX_VERTICAL],
		'qcm' => [_JEUX_QCM, _JEUX_QRM, _JEUX_QUIZ],
		'textes' => [_JEUX_POESIE, _JEUX_CITATION, _JEUX_BLAGUE],
		'devinettes' => [_JEUX_DEVINETTE, _JEUX_CHARADE],
		'trous' => [_JEUX_TROU],
		'pendu' => [_JEUX_PENDU],
		'diag_echecs' => [_JEUX_DIAG_ECHECS],
		'chesstuff' => [_JEUX_CHESSSTUFF],
		'saisies' => [_JEUX_SAISIE],
		'relier' => [_JEUX_GAUCHE],
	],

	// nom court a donner aux jeux
	'TYPES' => [
		'multi_jeux' => _T('jeux:multi_jeux'),
		'sudoku' => _T('sudoku:titre_court'),
		//'kakuro' => _T('kakuro:titre_court'),
		'mots_croises' => _T('motscroises:titre_court'),
		'qcm' => _T('qcm:titre_court'),
		'textes' => _L('Textes'),
		'devinettes' => _L('Devinettes'),
		'trous' => _L('Trous'),
		'pendu' => _T('pendu:titre_court'),
		'diag_echecs' => _L('Echecs'),
		'chesstuff' => _L('Echecs'),
		'saisies' => _L('Saisies'),
		'relier' => _L('Points à relier'),
	],

]; // $jeux_caracteristiques

// on envoie les caracteristiques aux plugins pour pouvoir ajouter des jeux tiers
$jeux_caracteristiques = pipeline('jeux_caracteristiques', $jeux_caracteristiques);

// addition de tous les separateurs
$temp = [];
foreach ($jeux_caracteristiques['SEPARATEURS'] as $sep) { $temp = array_merge($temp, $sep);
}
$jeux_caracteristiques['SEPARATEURS']['la_totale'] = array_unique($temp);
unset($temp);

// liste manuelle des css ou js a placer dans le header prive
// ca peut toujours servir pour les controles...
// dossiers : jeux/style/ et jeux/javascript/
global $jeux_header_prive, $jeux_javascript_prive;
$jeux_header_prive = ['jeux','qcm', 'mots_croises', 'sudoku', 'pendu', 'trous', 'relier'];
// mots_croises.js suffit car sudoku.js est a priori l'exacte copie
$jeux_javascript_prive = ['jeux', 'qcm', 'pendu', 'mots_croises', 'relier'];

// Codes RGB des couleurs predefinies a utiliser pour certains parametres apres la balise [config]
global $jeux_couleurs;
$jeux_couleurs = [
	// en
	'white' => [255,255,255],
	'black' => [0,0,0],
	'grey2' => [170,170,170],
	'grey' => [209,209,209],
	'green' => [191,220,192],
	'blue' => [152,192,218],
	'brown' => [224,183,153],
	'lightyellow' => [247,235,211],
	'lightbrown' => [255,243,217],
	'red' => [230,0,0],
	// fr
	'blanc' => [255,255,255],
	'noir' => [0,0,0],
	'gris2' => [170,170,170],
	'gris' => [209,209,209],
	'vert' => [191,220,192],
	'bleu' => [152,192,218],
	'brun' => [224,183,153],
	'jauneclair' => [247,235,211],
	'brunclair' => [255,243,217],
	'rouge' => [230,0,0],
];

function jeux_rgbArray($couleur) {
	global $jeux_couleurs;
	if (isset($jeux_couleurs[$couleur])) {
		return $jeux_couleurs[$couleur];
	}
	include_spip('inc/filtres_images_mini');
	$c = str_replace('html:', '', $couleur); // forcer une couleur html
	$c = _couleur_hex_to_dec($c);
	return $jeux_couleurs[$couleur] = [$c['red'], $c['green'], $c['blue']];
}

function jeux_rgb($couleur, $rgb = true) {
	list($a, $b, $c) = jeux_rgbArray($couleur);
	if ($rgb) { return "rgb($a, $b, $c)";
	}
	include_spip('inc/filtres_images_mini');
	return _couleur_dec_to_hex($a, $b, $c);
}

global $scoreMULTIJEUX;
$scoreMULTIJEUX = [];

// renvoie un tableau de lettres
function jeux_alphabet($alphabet = 'latin1', $br = false) {
	// surcharge ou extension eventuelle par constante. Ex: define('jeux_alphabet_voyelles', 'A,E,I,O,U');
	if (defined($a = 'jeux_alphabet_' . $alphabet)) {
		$res = constant($a);
	} else {
		// quelques alphabets connus (a completer ?)
		// le retour a la ligne est une double virgule
		switch ($alphabet) {
		case 'latin1':
			$res = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,-';
			break;
		case 'latinbr':
			$res = 'A,B,C,D,,E,F,G,H,I,,J,K,L,M,N,,O,P,Q,R,S,,T,U,V,W,X,,Y,Z,-';
			break;
		case 'azerty1':
			$res = 'A,Z,E,R,T,Y,U,I,O,P,,Q,S,D,F,G,H,J,K,L,M,,W,X,C,V,B,N';
			break;
		case 'azerty2':
			$res = 'A,Z,E,R,T,Y,U,I,O,P,,Q,S,D,F,G,H,J,K,L,M,,W,X,C,V,B,N,-';
			break;
		case 'qwerty1':
			$res = 'Q,W,E,R,T,Y,U,I,O,P,,A,S,D,F,G,H,J,K,L,,Z,X,C,V,B,N,M';
			break;
		case 'qwerty2':
			$res = 'Q,W,E,R,T,Y,U,I,O,P,,A,S,D,F,G,H,J,K,L,,Z,X,C,V,B,N,M,-';
			break;
		case 'qwerty3':
			$res = 'Q,W,E,R,T,Y,U,I,O,P,,A,S,D,F,G,H,J,K,L,Ñ,,Z,X,C,V,B,N,M,-';
			break;
		case 'español1':
			$res = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,Ñ,O,P,Q,R,S,T,U,V,W,X,Y,Z,-';
			break;
		case 'español2':
			$res = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,Ñ,O,P,Q,R,S,T,U,V,W,X,Y,Z,Á,É,Í,Ó,Ú,-';
			break;
		case 'cyrillic1':
			$res = 'А,Б,В,Г,Д,Е,Ё,Ж,З,И,Й,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Х,Ц,Ч,Ш,Щ,Ъ,Ь,Ы,Э,Ю,Я';
			break;
			// permet de retourner eventuellement un alphabet perso. Ex: jeux_alphabet('A,E,I,O,U')
		default:
			$res = $alphabet;
		}
	}
	// mb_regex_encoding('UTF-8'); return mb_split(',', $res);
	if (!$br) { return preg_split('/,+/u', $res);
	}
	$res = preg_split('/,,/u', $res);
	foreach ($res as $i => $v) { $res[$i] = preg_split('/,/u', $v);
	}
	return $res;
}
